import { expect } from 'chai';
import { getBondsData, cachedGetBondsData } from '../src/snippet2';


describe('getBondsData()', () => {
  async function multiply(calls, func) {
    const rt = [];
    for (let index = 0; index < calls.length; index++) {
      const item = await func(calls[index]);
      rt.push(item);
    }
    return rt;
  }

  it('testing without cache', (done) => {
    const isins = ['XS0971721963', 'RU000A0JU4L3'];
    const data = { date: '20180120', isins };

    multiply([data, data], getBondsData)
      .then((result) => {
        const [first, second] = result;

        first.forEach((item, index) => {
          expect(item.isin).equal(second[index].isin);
          expect(item.data).not.deep.equal(second[index].data); // not equal!
        });

        done();
      });
  });

  it('testing with cache', (done) => {
    const isins = ['XS0971721963', 'RU000A0JU4L3'];
    const data = { date: '20180120', isins };

    const cached = cachedGetBondsData();

    multiply([data, data], cached)
      .then((result) => {
        const [first, second] = result;

        first.forEach((item, index) => {
          expect(item.isin).equal(second[index].isin);
          expect(item.data).deep.equal(second[index].data); // equal = cached
        });

        done();
      });
  });
});
