class FakeHttp {
  async post(data) {
    const { url, body } = data;
    //
    return new Promise((resolve) => {
      const value = Math.round((2 ** 32) * Math.random());
      const response = body.map(item => ({ isin: item, data: { value } }));
      setTimeout(resolve, 100, response);
    });
  }
}

const http = new FakeHttp();

export const getBondsData = async ({ date, isins }) => {
  const result = await http.post({
    url: `/bonds/${date}`,
    body: isins,
  });
  return result;
};

/**
 * let getBondsData = cachedGetBondsData();
 * getBondsData.then(result => {
 *     console.log(result)
 * })
 */
export const cachedGetBondsData = () => {
  const cache = {};

  const getDateNode = (date) => {
    if (!cache[date]) { cache[date] = {}; }

    return cache[date];
  };

  const saveToCache = (date, result) => {
    const storeDate = getDateNode(date);
    result.forEach((item) => {
      storeDate[item.isin] = item.data;
    });
  };

  const getCache = (storeDate, isins) => isins.map(item => ({
    isin: item,
    data: storeDate[item],
  }));

  return async ({ date, isins }) => {
    const storeDate = getDateNode(date);
    const filteredIsins = isins.filter(item => !storeDate[item]);

    if (filteredIsins.length) {
      const result = await getBondsData({ date, isins: filteredIsins });
      saveToCache(date, result);
    }

    return getCache(storeDate, isins);
  };
};
